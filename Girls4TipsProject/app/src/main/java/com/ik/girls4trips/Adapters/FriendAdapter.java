package com.ik.girls4trips.Adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import com.ik.girls4trips.R;
import com.ik.girls4trips.databinding.FriendListItemsBinding;
import com.ik.girls4trips.models.DummyModel;

/**
 * Created by Urmila Jangir on 6/27/2018.
 */

public class FriendAdapter extends RecyclerView.Adapter<FriendAdapter.ViewHolder> {
    private Activity activity;
    private FriendAdapter.OnItemClickListener OnItemClickListener;
    List<DummyModel> dummyModelList;


    private boolean isLoadingAdded = false;

    public FriendAdapter(Activity activity, FriendAdapter.OnItemClickListener OnItemClickListener, List<DummyModel> dummyModelList) {
        this.activity = activity;
        this.OnItemClickListener = OnItemClickListener;
        this.dummyModelList = dummyModelList;
    }

    @Override
    public int getItemCount() {
        return dummyModelList.size();
    }

    @Override
    public FriendAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        FriendListItemsBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.friend_list_items, parent, false);

        return new FriendAdapter.ViewHolder(binding);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final FriendAdapter.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.binding.setItemClickListener(OnItemClickListener);
        holder.binding.setIndex(position);

        final DummyModel searchDataAll = dummyModelList.get(position);

    }


    class ViewHolder extends RecyclerView.ViewHolder {
        FriendListItemsBinding binding;

        ViewHolder(FriendListItemsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int index);

    }


}