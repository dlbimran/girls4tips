package com.ik.girls4trips.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.ik.girls4trips.Adapters.MyTripAdapter;
import com.ik.girls4trips.Adapters.TourPageAdapter;
import com.ik.girls4trips.R;
import com.ik.girls4trips.databinding.ActivityUserDetailsBinding;
import com.ik.girls4trips.fragments.TourPageFragment;

public class UserDetailActivity extends BaseActivity {

    private static final Integer[] IMAGES= {R.drawable.home,R.drawable.home, R.drawable.home, R.drawable.home, R.drawable.home, R.drawable.home};

    ActivityUserDetailsBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_details);

        binding.setActivity(this);

        TripsList();

        TourPageAdapter adapter = new TourPageAdapter(getSupportFragmentManager());
        for(int i =0; i <6; i++) {
            Bundle b = new Bundle();
            b.putInt("image", IMAGES[i]);
            b.putInt("pos", i);
            Fragment f = new TourPageFragment();
            f.setArguments(b);
            adapter.addFragment(f, "");
        }
        binding.pager.setAdapter(adapter);

       // binding.img.setImageResource(IMAGES[0]);

        binding.dotsTabs.setupWithViewPager(binding.pager);

        binding.dotsTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
              //  binding.img.setImageResource(IMAGES[binding.pager.getCurrentItem()]);

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public void onClick(View view) {
        switch (view.getId()){
            case R.id.menu:
                onBackPressed();
                break;

        }
    }
    void TripsList(){

        binding.tipsrecyclerview.setLayoutManager(new LinearLayoutManager(UserDetailActivity.this,LinearLayoutManager.HORIZONTAL,false));
        MyTripAdapter adapter1 = new MyTripAdapter();
        binding.tipsrecyclerview.setAdapter(adapter1);
    }
}
