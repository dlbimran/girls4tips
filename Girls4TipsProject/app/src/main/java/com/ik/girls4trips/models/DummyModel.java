package com.ik.girls4trips.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Urmila Jangir on 6/20/2018.
 */

public class DummyModel {
    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("consultancy_name")
    @Expose
    private String consultancy_name;

    @SerializedName("call")
    @Expose
    private String call;

    @SerializedName("email")
    @Expose
    private String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getConsultancy_name() {
        return consultancy_name;
    }

    public void setConsultancy_name(String consultancy_name) {
        this.consultancy_name = consultancy_name;
    }

    public String getCall() {
        return call;
    }

    public void setCall(String call) {
        this.call = call;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
