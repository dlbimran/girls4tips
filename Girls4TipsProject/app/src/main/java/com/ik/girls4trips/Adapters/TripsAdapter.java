package com.ik.girls4trips.Adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import com.ik.girls4trips.R;
import com.ik.girls4trips.databinding.TripsListItemBinding;
import com.ik.girls4trips.models.DummyModel;

/**
 * Created by Urmila Jangir on 6/30/2018.
 */

public class TripsAdapter extends RecyclerView.Adapter<TripsAdapter.ViewHolder> {
    private Activity activity;
    private TripsAdapter.OnItemClickListener OnItemClickListener;
    List<DummyModel> dummyModelList;
    String ScreenFrom ;

    public TripsAdapter(Activity activity, TripsAdapter.OnItemClickListener OnItemClickListener, List<DummyModel> dummyModelList, String ScreenFrom) {
        this.activity = activity;
        this.OnItemClickListener = OnItemClickListener;
        this.dummyModelList = dummyModelList;
        this.ScreenFrom = ScreenFrom;
    }

    @Override
    public int getItemCount() {
        return dummyModelList.size();
    }

    @Override
    public TripsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        TripsListItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.trips_list_item, parent, false);
        return new TripsAdapter.ViewHolder(binding);
    }
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final TripsAdapter.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.binding.setItemClickListener(OnItemClickListener);
        holder.binding.setIndex(position);

        if (ScreenFrom.equalsIgnoreCase("TripsMyFragment")) {
            holder.binding.profileHeader.setVisibility(View.GONE);
        }else {

        }
    }
    class ViewHolder extends RecyclerView.ViewHolder {
        TripsListItemBinding binding;
        ViewHolder(TripsListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
    public interface OnItemClickListener {
        void onItemClick(View view, int index);
    }
}