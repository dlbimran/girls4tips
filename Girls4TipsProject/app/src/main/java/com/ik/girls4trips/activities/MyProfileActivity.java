package com.ik.girls4trips.activities;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.ik.girls4trips.Adapters.MyPhotosAdapter;
import com.ik.girls4trips.Adapters.MyTripAdapter;
import com.ik.girls4trips.R;
import com.ik.girls4trips.databinding.ActivityMyProfileBinding;

public class MyProfileActivity extends AppCompatActivity {
    ActivityMyProfileBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_my_profile);
        binding.setActivity(this);
        init();
    }

    public void onClick(View view) {
        switch (view.getId()){
            case R.id.menu:
                onBackPressed();
                break;

        }
    }
    void init(){
        binding.toolbar.more.setVisibility(View.VISIBLE);
        binding.itemsRecyclerView.setLayoutManager(new LinearLayoutManager(MyProfileActivity.this,LinearLayoutManager.HORIZONTAL,false));
        MyPhotosAdapter adapter = new MyPhotosAdapter();
        binding.itemsRecyclerView.setAdapter(adapter);

        binding.RecyclerView.setLayoutManager(new LinearLayoutManager(MyProfileActivity.this,LinearLayoutManager.HORIZONTAL,false));
        MyTripAdapter adapter1 = new MyTripAdapter();
        binding.RecyclerView.setAdapter(adapter1);

    }
}
