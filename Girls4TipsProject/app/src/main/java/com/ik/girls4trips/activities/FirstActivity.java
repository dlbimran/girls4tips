package com.ik.girls4trips.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.ik.girls4trips.R;
import com.ik.girls4trips.databinding.ActivityFirstBinding;
import com.ik.girls4trips.extras.Utils;

public class FirstActivity extends BaseActivity {

    ActivityFirstBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_first);
        binding.setActivity(this);
    }

    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.login:
                if(binding.rlMainLayout.getVisibility() != View.VISIBLE){
                    Layoutanimation(binding.loginLayout.loginNested,binding.registerLayout.registerNested,Color.BLACK,getResources().getColor(R.color.black_99));
                    Utils.expand(binding.rlMainLayout);
                }else if(binding.loginLayout.loginNested.getVisibility() == View.VISIBLE){
                    Layoutanimation(binding.registerLayout.registerNested,binding.loginLayout.loginNested,Color.BLACK,getResources().getColor(R.color.black_99));
                    Utils.collapse(binding.rlMainLayout);
                }else{
                    Layoutanimation(binding.loginLayout.loginNested,binding.registerLayout.registerNested,Color.BLACK,getResources().getColor(R.color.black_99));
                }
                break;
            case R.id.register:
                if(binding.rlMainLayout.getVisibility() != View.VISIBLE){
                    Layoutanimation(binding.registerLayout.registerNested,binding.loginLayout.loginNested,getResources().getColor(R.color.black_99),Color.BLACK);
                    Utils.expand(binding.rlMainLayout);
                }else if(binding.registerLayout.registerNested.getVisibility() == View.VISIBLE){
                    Layoutanimation(binding.loginLayout.loginNested,binding.registerLayout.registerNested,getResources().getColor(R.color.black_99),Color.BLACK);
                    Utils.collapse(binding.rlMainLayout);
                }else{
                    Layoutanimation(binding.registerLayout.registerNested,binding.loginLayout.loginNested,getResources().getColor(R.color.black_99),Color.BLACK);
                }
                break;
            case R.id.tv_login:
                if(LoginvalidateAll()){
                    intent = new Intent(FirstActivity.this,MainActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    finish();
                }
                break;
            case R.id.forgot_password:
                intent = new Intent(FirstActivity.this,ForgotPasswordActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;
            case R.id.tv_register:
                if(validateAll()){
                    intent = new Intent(FirstActivity.this,MainActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
                break;
        }
    }
    void Layoutanimation(NestedScrollView layout1, NestedScrollView layout2, int black, int trans){
        Utils.expandWidth(layout1);
        Utils.collapseWidth(layout2);
        binding.login.setBackgroundColor(black);
        binding.register.setBackgroundColor(trans);
    }
    public boolean LoginvalidateAll() {
        boolean validation = true;
        if (!Utils.isEmailValid(binding.loginLayout.email.getText().toString().trim())) {
            toast(FirstActivity.this, getResources().getString(R.string.valid_email_address));
            validation = false;
        }else if(!Utils.isValidPassword(binding.loginLayout.password.getText().toString().trim())){
            toast(FirstActivity.this, getResources().getString(R.string.valid_password));
            validation = false;
        }
        return validation;
    }
    public boolean validateAll() {
        boolean validation = true;
        if (!Utils.validateString(binding.registerLayout.fullName.getText().toString().trim())) {
            toast(FirstActivity.this, getResources().getString(R.string.name_validation));
            validation = false;
        }else  if (!Utils.validateString(binding.registerLayout.age.getText().toString().trim())) {
            toast(FirstActivity.this, getResources().getString(R.string.age_validation));
            validation = false;
        }else if (!Utils.isEmailValid(binding.registerLayout.email.getText().toString().trim())) {
            toast(FirstActivity.this, getResources().getString(R.string.valid_email_address));
            validation = false;
        }else if(!Utils.isValidPassword(binding.registerLayout.password.getText().toString().trim())){
            toast(FirstActivity.this, getResources().getString(R.string.valid_password));
            validation = false;
        }
        return validation;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_top);
        finish();
    }
}