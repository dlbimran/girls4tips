package com.ik.girls4trips.activities;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.ik.girls4trips.R;
import com.ik.girls4trips.databinding.ActivityMembershipBinding;

import static android.graphics.Color.GRAY;

public class MembershipActivity extends BaseActivity {
    ActivityMembershipBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_membership);

        init();
    }


    private void init() {
        binding.mToolbar1.tvMenuName.setText(getResources().getString(R.string.premium_membership));
       // support
        SpannableString span=new SpannableString(getResources().getString(R.string.support));
        span.setSpan(new RelativeSizeSpan(0.80f), 19, 43, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        span.setSpan(new RelativeSizeSpan(0.80f), 53, span.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        span.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.terms)), 19, 43, 0);
        span.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.terms)), 53, span.length(), 0);
        binding.tvSupport.setText(span);
    }

    public void onClick(View view) {
        switch (view.getId()){
            case R.id.menu:
                onBackPressed();
                break;

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_top);
        finish();
    }
}
