package com.ik.girls4trips.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ik.girls4trips.R;
import com.ik.girls4trips.databinding.FragmentProfileBinding;


public class ProfileFragment extends BaseFragment {
    FragmentProfileBinding binding;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
        binding.setFragment(this);

        return binding.getRoot();
    }

}
