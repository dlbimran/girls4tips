package com.ik.girls4trips.fragments;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import com.ik.girls4trips.Adapters.StoryAdapter;
import com.ik.girls4trips.Adapters.ViewPagerAdapter;
import com.ik.girls4trips.R;
import com.ik.girls4trips.activities.UserDetailActivity;
import com.ik.girls4trips.databinding.FragmentDefaultBinding;
import com.ik.girls4trips.models.DummyModel;


public class DefaultFragment extends BaseFragment implements StoryAdapter.OnItemClickListener {
    FragmentDefaultBinding binding;
    DummyModel dummyModel;
    List<DummyModel> dummyModelList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_default, container, false);
        binding.setFragment(this);

        for (int i = 0; i < 10; i++) {
            dummyModel = new DummyModel();
            dummyModel.setCall("1" + i);
            dummyModelList.add(dummyModel);
        }

        binding.itemsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
        StoryAdapter storyAdapter = new StoryAdapter(getActivity(), this, dummyModelList);
        binding.itemsRecyclerView.setNestedScrollingEnabled(false);
        binding.itemsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        binding.itemsRecyclerView.setAdapter(storyAdapter);

        ViewPagerAdapter adapterr = new ViewPagerAdapter(getActivity());
        binding.pagerMain.setAdapter(adapterr);

        return binding.getRoot();
    }


    @Override
    public void onItemClick(View view, int index) {

        Intent intent = new Intent(getActivity() , UserDetailActivity.class);
        startActivity(intent);

    }
}
