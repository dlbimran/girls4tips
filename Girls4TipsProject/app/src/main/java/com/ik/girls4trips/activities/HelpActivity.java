package com.ik.girls4trips.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.ik.girls4trips.R;
import com.ik.girls4trips.databinding.ActivityHelpBinding;
import com.ik.girls4trips.extras.Constants;

public class HelpActivity extends BaseActivity {
    ActivityHelpBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_help);
        binding.setActivity(this);

        init();
    }

    private void init() {
        binding.mToolbar1.tvMenuName.setText(getResources().getString(R.string.help));
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.menu:
                onBackPressed();
                break;
            case R.id.contact_us:
                Intent i1 = new Intent(getApplicationContext(), WebViewActivity.class);
                i1.putExtra("URL", Constants.BASE_URL_CONTACT);
                i1.putExtra("TITLE", getResources().getString(R.string.contact_us));
                startActivity(i1);


                break;
            case R.id.privacy_policy:
                i1 = new Intent(getApplicationContext(), WebViewActivity.class);
                i1.putExtra("URL", Constants.BASE_URL_PRIVACYPOLICY);
                i1.putExtra("TITLE", getResources().getString(R.string.privacy_policy));
                startActivity(i1);

                break;
            case R.id.terms_condi:
                i1 = new Intent(getApplicationContext(), WebViewActivity.class);
                i1.putExtra("URL", Constants.BASE_URL_TERMS);
                i1.putExtra("TITLE", getResources().getString(R.string.term_and_condition));
                startActivity(i1);
                break;
            case R.id.about_us:
                i1 = new Intent(getApplicationContext(), WebViewActivity.class);
                i1.putExtra("URL", Constants.BASE_URL_ABOUTUS);
                i1.putExtra("TITLE", getResources().getString(R.string.about_me));
                startActivity(i1);
                break;
            case R.id.disclaimer:
                i1 = new Intent(getApplicationContext(), WebViewActivity.class);
                i1.putExtra("URL", Constants.BASE_URL_DISCLEMA);
                i1.putExtra("TITLE", getResources().getString(R.string.disclaimer));
                startActivity(i1);
                break;

        }
    }

    void ButtnoColor(){

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_top);
        finish();
    }
}
