package com.ik.girls4trips.extras;

import android.content.Context;
import android.content.SharedPreferences;


public class AppPreferenceManager {

    public static final String PREF_NAME = "mySharePrefwifistudy";
    public static final String PREF_FCM_ID= "fcm_id";
    public static final String USER_ID= "user_id";
    public static final String USER_TYPE= "seeker";
    public static final String USER_Name= "name";
    public static final String USER_IMAGE= "profile_img";
    public static final String USER_Email= "email";
    public static final String USER_lang= "lang";
    public static final String THEME= "NIGHT_MODE";

    private static SharedPreferences mSharedPreferences;

    public static SharedPreferences getSharedPreferences(Context context) {
        if (mSharedPreferences == null) {
            mSharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        }
        return mSharedPreferences;
    }

    public static void setPushRegId(Context context, String id) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_FCM_ID, id);
        editor.commit();
    }
    public static void setUserName(Context context, String id) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(USER_Name, id);
        editor.commit();
    }

    public static void setUserEmail(Context context, String id) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(USER_Email, id);
        editor.commit();
    }
    public static void setUserProfileImg(Context context, String id) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(USER_IMAGE, id);
        editor.commit();
    }

    public static void setUserLang(Context context, String id) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(USER_lang, id);
        editor.commit();
    }
    public static void setUserId(Context context, String id) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(USER_ID, id);
        editor.commit();
    }
    public static void setUserType(Context context, String id) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(USER_TYPE, id);
        editor.commit();
    }

    public static void setTheme(Context context, String id) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(THEME, id);
        editor.commit();
    }

    public static String getPushRegId(Context context) {
        return getSharedPreferences(context).getString(PREF_FCM_ID, "");
    }
    public static String getUserId(Context context) {
        return getSharedPreferences(context).getString(USER_ID, "");
    }
    public static String getUserEmail(Context context) {
        return getSharedPreferences(context).getString(USER_Email, "");
    }
     public static String getUserProfileImg(Context context) {
        return getSharedPreferences(context).getString(USER_IMAGE, "");
    }
    public static String getUserType(Context context) {
        return getSharedPreferences(context).getString(USER_TYPE, "");
    }
    public static String getUserName(Context context) {
        return getSharedPreferences(context).getString(USER_Name, "");
    }
    public static String getUserLang(Context context) {
        return getSharedPreferences(context).getString(USER_lang, "");
    }
    public static void clearAll(Context context) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.clear();
        editor.commit();
    }

    public static String getTheme(Context context) {
        return getSharedPreferences(context).getString(THEME, "");
    }



    private SharedPreferences sharePreference;
    SharedPreferences.Editor editor;

    public static String PAUSEBUTTON = "pausebutton";
    private static AppPreferenceManager mySession;

    public AppPreferenceManager(Context context) {
        String sharePrefName = "mySharePrefwifistudy";
        sharePreference = context.getSharedPreferences(sharePrefName, Context.MODE_PRIVATE);
        editor = sharePreference.edit();
    }
    public static AppPreferenceManager getInstance(Context context) {

        if (mySession == null) {
            mySession = new AppPreferenceManager(context);
        }
        return mySession;
    }

    public String getString(String key) {

        return sharePreference.getString(key, "");
    }

    public void putString(String key, String value) {

        editor.putString(key, value);
        editor.commit();
    }

   public void putBoolean(String key, Boolean value) {

        editor.putBoolean(key, value);
        editor.commit();
    }

    public boolean getBoolean(String key) {

        return sharePreference.getBoolean(key, false);
    }


    public void prefDeleteKey(String Key) {
        sharePreference.edit().remove(Key).commit();

    }
    public boolean HasInt(String key) {
        return sharePreference.contains(key);
    }
}
