package com.ik.girls4trips.binding;

import android.databinding.BindingAdapter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.TextView;

import recyclerview.DividerItemDecoration;


/**
 * Created by Vaibhav Agarwal on 01/12/2018.
 */

public class DataBindingAdapter {

    @BindingAdapter({"bind:font"})
    public static void setFont(TextView textView, String fontName) {
        try {
            textView.setTypeface(Typeface.createFromAsset(textView.getContext().getAssets(), "fonts/" + fontName));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @BindingAdapter({"bind:et_font"})
    public static void setFont(EditText editText, String fontName) {
        try {
            editText.setTypeface(Typeface.createFromAsset(editText.getContext().getAssets(), "fonts/" + fontName));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @BindingAdapter({"bind:orientation", "bind:divider"})
    //set orientation Horizontal(0) or Vertical(1)
    public static void setDivider(RecyclerView recyclerView, int orientation, Drawable divider) {
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), orientation, divider));
    }


}
