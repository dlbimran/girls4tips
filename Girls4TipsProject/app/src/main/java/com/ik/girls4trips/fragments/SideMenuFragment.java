package com.ik.girls4trips.fragments;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ik.girls4trips.R;
import com.ik.girls4trips.activities.FriendsActivity;
import com.ik.girls4trips.activities.MainActivity;
import com.ik.girls4trips.activities.MembershipActivity;
import com.ik.girls4trips.activities.MyProfileActivity;
import com.ik.girls4trips.activities.ProfileInfoActivity;
import com.ik.girls4trips.activities.SettingActivity;
import com.ik.girls4trips.activities.TripsActivity;
import com.ik.girls4trips.databinding.FragmentSideMenuBinding;


public class SideMenuFragment extends BaseFragment {

     FragmentSideMenuBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_side_menu, container, false);
        binding.setFragment(this);

        return binding.getRoot();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void onClick(View v) {
        Intent i;
        switch (v.getId()) {
            case R.id.ly_header:
                i = new Intent(getActivity(), MyProfileActivity.class);
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                getParentActivity().getDrawer().closeDrawers();
                break;
            case R.id.near_me:
                i = new Intent(getActivity(), FriendsActivity.class);
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                getParentActivity().getDrawer().closeDrawers();
                break;
            case R.id.membership:
                i = new Intent(getActivity(), MembershipActivity.class);
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                getParentActivity().getDrawer().closeDrawers();
                break;
            case R.id.setting:
                i = new Intent(getActivity(), SettingActivity.class);
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                getParentActivity().getDrawer().closeDrawers();
                break;
            case R.id.trip:
                i = new Intent(getActivity(), TripsActivity.class);
                startActivity(i);
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                getParentActivity().getDrawer().closeDrawers();
                break;

        }
    }
    private MainActivity getParentActivity() {
        return (MainActivity) getActivity();
    }
}
