package com.ik.girls4trips.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ik.girls4trips.R;
import com.ik.girls4trips.databinding.FragmentTourBinding;

/**
 * Created by Vaibhav Agarwal on 1/12/2018.
 */

public class TourPageFragment extends Fragment {

    FragmentTourBinding binding;
    int image;

    private static final Integer[] IMAGES= {R.drawable.home,R.drawable.home, R.drawable.home, R.drawable.home, R.drawable.home, R.drawable.home};

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       if(getArguments()!=null && getArguments().containsKey("pos")) {
            image= getArguments().getInt("pos");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_tour, container, false);
        binding.setFragment(this);
        binding.img.setImageResource(IMAGES[image]);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
