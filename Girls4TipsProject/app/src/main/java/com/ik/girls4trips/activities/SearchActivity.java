package com.ik.girls4trips.activities;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.ik.girls4trips.Adapters.MyTripAdapter;
import com.ik.girls4trips.R;
import com.ik.girls4trips.databinding.ActivitySearchBinding;

public class SearchActivity extends AppCompatActivity {

    ActivitySearchBinding binding ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_search);

        binding.rangeSeekbar1.setMinValue(6).setMaxValue(30).setMinStartValue(7).setMaxStartValue(10).apply();

        binding.cityRecyclerView.setLayoutManager(new LinearLayoutManager(SearchActivity.this,LinearLayoutManager.HORIZONTAL,false));
        MyTripAdapter adapter1 = new MyTripAdapter();
        binding.cityRecyclerView.setAdapter(adapter1);



        binding.toolbar.tvMenuName.setText("Search");
        binding.toolbar.more.setImageResource(R.drawable.close);

    }

    public void onClick(View view) {
        switch (view.getId()){
            case R.id.menu:
                onBackPressed();
                break;

        }
    }
}
