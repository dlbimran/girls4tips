package com.ik.girls4trips.Adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import com.ik.girls4trips.R;
import com.ik.girls4trips.databinding.ListConvoReceivedItemBinding;
import com.ik.girls4trips.extras.ViewBinderHelper;
import com.ik.girls4trips.models.DummyModel;

/**
 * Created by Urmila Jangir on 6/29/2018.
 */

public class ConvoReceivedAdapter extends RecyclerView.Adapter<ConvoReceivedAdapter.ViewHolder>{
    private Activity activity;
    private final ViewBinderHelper binderHelper = new ViewBinderHelper();
    private OnItemClickListener OnItemClickListener;
    List<DummyModel> dummyModelList;

    private boolean isLoadingAdded = false;

    public ConvoReceivedAdapter(Activity activity, OnItemClickListener OnItemClickListener, List<DummyModel> dummyModelList) {
        this.activity=activity;
        this.OnItemClickListener=OnItemClickListener;
        this.dummyModelList=dummyModelList;
    }

    @Override
    public int getItemCount() {
        return dummyModelList.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ListConvoReceivedItemBinding binding= DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.list_convo_received_item,parent,false);

        return new ViewHolder(binding);
    }
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        /*Drawable delete= Utils.DrawableChange(activity,R.drawable.delete_white, Color.BLACK);
        holder.binding.infoButton.setImageDrawable(delete);*/
        binderHelper.bind(holder.binding.swipeLayout, String.valueOf(position));
        holder.binding.setItemClickListener(OnItemClickListener);
        holder.binding.setIndex(position);

        holder.binding.infoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                        remove(String.valueOf(position));
            }
        });
    }

    public void remove(String item) {
        int position = Integer.parseInt(item);
        dummyModelList.remove(position);
        notifyItemRemoved(position);
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        ListConvoReceivedItemBinding binding;
        ViewHolder(ListConvoReceivedItemBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }
    }
    public interface OnItemClickListener {
        void onItemClick(View view, int index);

    }


}