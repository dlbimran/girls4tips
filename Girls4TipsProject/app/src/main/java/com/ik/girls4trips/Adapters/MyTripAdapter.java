package com.ik.girls4trips.Adapters;

import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ik.girls4trips.R;
import com.ik.girls4trips.databinding.ProfilePhotoBinding;
import com.ik.girls4trips.databinding.TripPhotoBinding;

public class MyTripAdapter extends RecyclerView.Adapter<MyTripAdapter.ViewHolder> {

    public MyTripAdapter() { }

    @Override
    public int getItemCount() {
        return 4;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewDataBinding binding= DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.trip_photo,parent,false);
        return  new ViewHolder(binding);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        TripPhotoBinding binding=(TripPhotoBinding)holder.getBinding();
        if(position == 3){
            binding.morePhoto.setVisibility(View.VISIBLE);
            binding.tripItem.setVisibility(View.GONE);
        }else {
            binding.morePhoto.setVisibility(View.GONE );
            binding.tripItem.setVisibility(View.VISIBLE);
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ViewDataBinding binding;
        ViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
            this.binding.executePendingBindings();
        }
        public ViewDataBinding getBinding() {
            return binding;
        }
    }
}