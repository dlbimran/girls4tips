package com.ik.girls4trips.extras;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatDelegate;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class Constants {

    public static final long SPLASH_TIME=2500;
    public static final String BASE_URL =  "https://hicounselor.com/app/";
    public static final String BASE_URL_IMAGE =  "https://hicounselor.com/";

    public static final String BASE_URL_CONTACT =  "https://www.girls4trips.com/contact-us";
    public static final String BASE_URL_PRIVACYPOLICY =  "https://www.girls4trips.com/privacy-policy";
    public static final String BASE_URL_TERMS =  "https://www.girls4trips.com/terms-and-conditions";
    public static final String BASE_URL_ABOUTUS =  "https://www.girls4trips.com/about-us";
    public static final String BASE_URL_DISCLEMA=  "https://www.girls4trips.com/disclaimer";


    public static void hideKeyboard(Activity activity) {
        View view = activity.findViewById(android.R.id.content);
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void checkThemeColor(Activity activity) {
        if (AppPreferenceManager.getTheme(activity).equalsIgnoreCase("2")) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
    }
    public static String convert_time(int n){
        return n < 10 ? "0" + n : "" + n;
    }


    public static String convert(int n){
        return n < 10 ? "0" + n : "" + n;
    }
}
