package com.ik.girls4trips.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ik.girls4trips.R;
import com.ik.girls4trips.databinding.ActivityMainBinding;
import com.ik.girls4trips.fragments.DefaultFragment;

public class MainActivity extends BaseActivity {

    ActivityMainBinding binding;
    int pagerPos = 4;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setActivity(this);


        Fragment fragment = new DefaultFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_frame, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
        init();

        /*binding.tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Fragment fragment;
                TextView img = (TextView) ((RelativeLayout) tab.getCustomView()).getChildAt(0);
                Drawable d;
                switch(tab.getPosition()) {
                    case 0:
                        d= Utils.DrawableChange(MainActivity.this,R.drawable.friends,getResources().getColor(R.color.pink_button));
                        img.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.friends,0,0);
                        img.setTextColor(getResources().getColor(R.color.pink_button));
                        fragment = new LikeFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_frame, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
                        break;
                    case 1:
                        d= Utils.DrawableChange(MainActivity.this,R.drawable.message,getResources().getColor(R.color.pink_button));
                        img.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.message,0,0);
                        img.setTextColor(getResources().getColor(R.color.pink_button));
                        Intent intent = new Intent(MainActivity.this,ConversationActivity.class);
                        startActivity(intent);
                        break;
                    case 2:
                        d= Utils.DrawableChange(MainActivity.this,R.drawable.chat,getResources().getColor(R.color.pink_button));
                        img.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.views,0,0);
                        img.setTextColor(getResources().getColor(R.color.pink_button));
                        fragment = new ViewFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_frame, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
                        break;
                    case 3:
                        d= Utils.DrawableChange(MainActivity.this,R.drawable.help,getResources().getColor(R.color.pink_button));
                        img.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.help,0,0);
                        img.setTextColor(getResources().getColor(R.color.pink_button));
                        fragment = new ProfileFragment();
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_frame, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                TextView img = (TextView) ((RelativeLayout) tab.getCustomView()).getChildAt(0);
                Drawable d;
                switch(tab.getPosition()) {
                    case 0:
                        d= Utils.DrawableChange(MainActivity.this,R.drawable.ic_favorite,getResources().getColor(R.color.gray_dark));
                        img.setCompoundDrawablesWithIntrinsicBounds(null,d,null,null);
                        img.setTextColor(getResources().getColor(R.color.gray_dark));
                        break;
                    case 1:
                        d= Utils.DrawableChange(MainActivity.this,R.drawable.chat,getResources().getColor(R.color.gray_dark));
                        img.setCompoundDrawablesWithIntrinsicBounds(null,d,null,null);
                        img.setTextColor(getResources().getColor(R.color.gray_dark));
                        break;
                    case 2:
                        d= Utils.DrawableChange(MainActivity.this,R.drawable.chat,getResources().getColor(R.color.gray_dark));
                        img.setCompoundDrawablesWithIntrinsicBounds(null,d,null,null);
                        img.setTextColor(getResources().getColor(R.color.gray_dark));
                        break;
                    case 3:
                        d= Utils.DrawableChange(MainActivity.this,R.drawable.help,getResources().getColor(R.color.gray_dark));
                        img.setCompoundDrawablesWithIntrinsicBounds(null,d,null,null);
                        img.setTextColor(getResources().getColor(R.color.gray_dark));
                        break;
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });*/
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void init() {
        try {
            RelativeLayout ll_two = (RelativeLayout) LayoutInflater.from(MainActivity.this).inflate(R.layout.tab_custom, null);
            binding.tabs.addTab(binding.tabs.newTab(), false);
            ((TextView) ll_two.findViewById(R.id.name)).setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_favorite, 0, 0);
            ((TextView) ll_two.findViewById(R.id.name)).setText("Friends");
            binding.tabs.getTabAt(0).setCustomView(ll_two);
            binding.tabs.getTabAt(0).setTag(getResources().getString(R.string.like));

            RelativeLayout ll_three = (RelativeLayout) LayoutInflater.from(MainActivity.this).inflate(R.layout.tab_custom, null);
            binding.tabs.addTab(binding.tabs.newTab(), false);
            ((TextView) ll_three.findViewById(R.id.name)).setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.chat, 0, 0);
            ((TextView) ll_three.findViewById(R.id.name)).setText("Messages");
            binding.tabs.getTabAt(1).setCustomView(ll_three);
            binding.tabs.getTabAt(1).setTag(getResources().getString(R.string.message));

            RelativeLayout ll_four = (RelativeLayout) LayoutInflater.from(MainActivity.this).inflate(R.layout.tab_custom, null);
            binding.tabs.addTab(binding.tabs.newTab(), false);
            ((TextView) ll_four.findViewById(R.id.name)).setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.chat, 0, 0);
            ((TextView) ll_four.findViewById(R.id.name)).setText("Views");
            binding.tabs.getTabAt(2).setCustomView(ll_four);
            binding.tabs.getTabAt(2).setTag(getResources().getString(R.string.views));

            RelativeLayout ll_five = (RelativeLayout) LayoutInflater.from(MainActivity.this).inflate(R.layout.tab_custom, null);
            binding.tabs.addTab(binding.tabs.newTab(), false);
            ((TextView) ll_five.findViewById(R.id.name)).setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.help, 0, 0);
            ((TextView) ll_five.findViewById(R.id.name)).setText("Help");
            binding.tabs.getTabAt(3).setCustomView(ll_five);
            binding.tabs.getTabAt(3).setTag(getResources().getString(R.string.profile));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.menu:
                binding.drawer.openDrawer(GravityCompat.START);
                break;
            case R.id.tabFriends:
                Intent intent = new Intent(MainActivity.this, FriendsActivity.class);
                startActivity(intent);
                break;
            case R.id.tabMessages:
                intent = new Intent(MainActivity.this, ConversationActivity.class);
                startActivity(intent);
                break;
            case R.id.tabViews:
                intent = new Intent(MainActivity.this, ViewdByActivity.class);
                startActivity(intent);
                break;
            case R.id.tabHelp:
                intent = new Intent(MainActivity.this, HelpActivity.class);
                startActivity(intent);
                break;

                case R.id.search:
                intent = new Intent(MainActivity.this, SearchActivity.class);
                startActivity(intent);
                break;
        }
    }



    @Override
    public void onBackPressed() {
        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() == 0) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

    public DrawerLayout getDrawer() {
        return binding.drawer;
    }
}
