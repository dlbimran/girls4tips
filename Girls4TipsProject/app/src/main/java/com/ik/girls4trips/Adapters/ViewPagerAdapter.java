package com.ik.girls4trips.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.SnapHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ik.girls4trips.R;
import com.ik.girls4trips.activities.ConversationActivity;
import com.ik.girls4trips.activities.FriendsActivity;
import com.ik.girls4trips.activities.MainActivity;
import com.ik.girls4trips.activities.UserDetailActivity;
import com.ik.girls4trips.databinding.PagerItemBinding;
import com.ik.girls4trips.extras.VerticalViewPager;

public class ViewPagerAdapter extends PagerAdapter {
    private Activity activity;
    public ViewPagerAdapter(Activity activity) {
        this.activity = activity;
    }
    @Override
    public int getCount() {
        return 10 ;
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
    }
    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public Object instantiateItem(View collection, final int position) {
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        PagerItemBinding binding= DataBindingUtil.inflate(inflater,R.layout.pager_item,null,false);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL,false));
        binding.recyclerView.setHasFixedSize(true);
        HomeRecyclerAdapter adapter=new HomeRecyclerAdapter();
        binding.recyclerView.setAdapter(adapter);
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(binding.recyclerView);
        ((VerticalViewPager) collection).addView(binding.getRoot());
        binding.lyMainCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, UserDetailActivity.class);
                activity.startActivity(intent);
            }
        });

        binding.message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, ConversationActivity.class);
                activity.startActivity(intent);
            }
        });
        binding.like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, FriendsActivity.class);
                activity.startActivity(intent);
            }
        });
        binding.gallary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return binding.getRoot();
    }
    @Override
    public void destroyItem(View collection, int position, Object view) {
        ((VerticalViewPager) collection).removeView((View) view);
    }
}