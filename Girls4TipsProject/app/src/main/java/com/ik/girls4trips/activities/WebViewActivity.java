package com.ik.girls4trips.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.ik.girls4trips.R;
import com.ik.girls4trips.databinding.ActivityWebViewBinding;




public class WebViewActivity extends BaseActivity{

    ActivityWebViewBinding binding;
    String url="";
    String title="";
    boolean flag = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_web_view);
        binding.setActivity(this);

        if(getIntent()!=null) {
            if(getIntent().hasExtra("URL")) {
                url= getIntent().getStringExtra("URL");
            }
            if(getIntent().hasExtra("TITLE")) {
                title = getIntent().getStringExtra("TITLE");
            }
        }

        if (url.contains("http")) {
            binding.mToolbar.tvMenuName.setVisibility(View.VISIBLE);
        }else {
            binding.mToolbar.tvMenuName.setVisibility(View.GONE);
        }


        binding.mToolbar.menu.setVisibility(View.VISIBLE);
        binding.mToolbar.tvMenuName.setText(title);

        binding.mToolbar.menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        binding.mToolbar.tvMenuName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        binding.web.getSettings().setJavaScriptEnabled(true);
        binding.web.setWebViewClient(mWebViewClient);
        binding.web.loadUrl(url);

    }

    WebViewClient mWebViewClient = new WebViewClient() {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            enableLoadingBar(WebViewActivity.this, true,getString(R.string.loading));
        }
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String RequestUrl) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(RequestUrl));
            startActivity(browserIntent);
            return true;
        }
        @Override
        public void onPageFinished(WebView view, String url) {
            enableLoadingBar(WebViewActivity.this, false, getString(R.string.loading));
          //  binding.mToolbar.title.setText(view.getTitle());
        }
    };
}
