package com.ik.girls4trips.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.ik.girls4trips.databinding.ActivityForgotPasswordBinding;
import com.ik.girls4trips.R;

public class ForgotPasswordActivity extends BaseActivity {
    ActivityForgotPasswordBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_forgot_password);
        binding.setActivity(this);
    }
    public void onClick(View v){
        switch (v.getId()){
            case R.id.send:
                finish();
                break;
        }
    }
}
