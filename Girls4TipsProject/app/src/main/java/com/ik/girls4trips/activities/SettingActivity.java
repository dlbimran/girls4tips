package com.ik.girls4trips.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.ik.girls4trips.R;
import com.ik.girls4trips.databinding.ActivitySettingBinding;

public class SettingActivity extends BaseActivity {
    ActivitySettingBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_setting);
        binding.setActivity(this);
        init();
    }

    private void init() {
        binding.mToolbar1.tvMenuName.setText(getResources().getString(R.string.setting));
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.menu:
                onBackPressed();
                break;
            case R.id.rl_profile_info:
                Intent intent = new Intent (getApplicationContext(), ProfileInfoActivity.class);
                startActivity(intent);
                break;
            case R.id.rl_phone:
                 intent = new Intent (getApplicationContext(), EditPhoneActivity.class);
                startActivity(intent);
                break;
            case R.id.rl_email:
                 intent = new Intent (getApplicationContext(), EditEmailActivity.class);
                startActivity(intent);
                break;
            case R.id.rl_change_password:
                 intent = new Intent (getApplicationContext(), ChangePasswordActivity.class);
                startActivity(intent);
                break;
            case R.id.rl_referral:
                 intent = new Intent (getApplicationContext(), ReferralActivity.class);
                startActivity(intent);
                break;
            case R.id.rl_remove_acc:

                break;

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_top);
        finish();
    }
}