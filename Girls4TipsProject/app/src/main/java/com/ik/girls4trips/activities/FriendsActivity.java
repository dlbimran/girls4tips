package com.ik.girls4trips.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.List;

import com.ik.girls4trips.Adapters.FriendAdapter;
import com.ik.girls4trips.R;
import com.ik.girls4trips.databinding.ActivityFriendsBinding;
import com.ik.girls4trips.models.DummyModel;

public class FriendsActivity extends BaseActivity implements FriendAdapter.OnItemClickListener {

    ActivityFriendsBinding binding;
    DummyModel dummyModel;
    List<DummyModel> dummyModelList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_friends);
        binding.setActivity(this);

        for (int i = 0; i < 20; i++) {
            dummyModel = new DummyModel();
            dummyModel.setCall("1" + i);
            dummyModelList.add(dummyModel);
        }

        binding.itemsRecyclerView.setLayoutManager(new GridLayoutManager(this,2));
        //binding.itemsRecyclerView.setHasFixedSize(true);
        FriendAdapter friendAdapter = new FriendAdapter(this, this, dummyModelList);
        binding.itemsRecyclerView.setNestedScrollingEnabled(false);
        binding.itemsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        binding.itemsRecyclerView.setAdapter(friendAdapter);

        init();
    }
    private void init() {
        binding.mToolbar1.tvMenuName.setText(getResources().getString(R.string.friends));
    }

    public void onClick(View view) {
        switch (view.getId()){
            case R.id.menu:
                onBackPressed();
                break;

        }
    }

    @Override
    public void onItemClick(View view, int index) {

        Intent intent   = new Intent(FriendsActivity.this , UserDetailActivity.class);
        startActivity(intent);

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_top);
        finish();
    }

}
