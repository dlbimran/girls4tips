package com.ik.girls4trips.activities;

import android.databinding.DataBindingUtil;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ik.girls4trips.Adapters.HomePagerAdapter;
import com.ik.girls4trips.R;
import com.ik.girls4trips.databinding.ActivityTripsBinding;
import com.ik.girls4trips.fragments.TripsAllFragment;
import com.ik.girls4trips.fragments.TripsMyFragment;

public class TripsActivity extends BaseActivity {
    ActivityTripsBinding binding;
    int pagerPos=0;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_trips);
        binding.setActivity(this);

        init();
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void init() {
        binding.mToolbar1.tvMenuName.setText(getResources().getString(R.string.trips));
        setupViewPager(binding.viewpager);
    }

    public void onClick(View view) {
        switch (view.getId()){
            case R.id.menu:
                onBackPressed();
                break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setupViewPager(ViewPager viewPager) {

        HomePagerAdapter adapter = new HomePagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new TripsMyFragment(), getResources().getString(R.string.trips_my));
        adapter.addFragment(new TripsAllFragment(), getResources().getString(R.string.trips_all));

        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(pagerPos);
        viewPager.setOffscreenPageLimit(3);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.tabs));
        binding.tabs.setupWithViewPager(viewPager);
        try {
            RelativeLayout ll_one = (RelativeLayout) LayoutInflater.from(TripsActivity.this).inflate(R.layout.tab_custom1, null);
            ((TextView) ll_one.findViewById(R.id.name)).setText(getResources().getString(R.string.trips_my));
            binding.tabs.getTabAt(0).setCustomView(ll_one);
            binding.tabs.getTabAt(0).setTag(getResources().getString(R.string.trips_my));

            RelativeLayout ll_two = (RelativeLayout) LayoutInflater.from(TripsActivity.this).inflate(R.layout.tab_custom1, null);
            ((TextView) ll_two.findViewById(R.id.name)).setText(getResources().getString(R.string.trips_all));
            binding.tabs.getTabAt(1).setCustomView(ll_two);
            binding.tabs.getTabAt(1).setTag(getResources().getString(R.string.trips_all));

        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_top);
        finish();
    }
}
