package com.ik.girls4trips.fragments;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import com.ik.girls4trips.Adapters.TripsAdapter;
import com.ik.girls4trips.R;
import com.ik.girls4trips.activities.FriendsActivity;
import com.ik.girls4trips.activities.UserDetailActivity;
import com.ik.girls4trips.databinding.FragmentTripsMyBinding;
import com.ik.girls4trips.models.DummyModel;


public class TripsMyFragment extends BaseFragment implements TripsAdapter.OnItemClickListener {
    FragmentTripsMyBinding binding;
    DummyModel dummyModel;
    List<DummyModel> dummyModelList = new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_trips_my, container, false);
        binding.setFragment(this);

        for (int i = 0; i < 20; i++) {
            dummyModel = new DummyModel();
            dummyModel.setCall("1" + i);
            dummyModelList.add(dummyModel);
        }

        binding.itemsRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));
        //binding.itemsRecyclerView.setHasFixedSize(true);
        TripsAdapter tripsAdapter = new TripsAdapter(getActivity(), this, dummyModelList, TripsMyFragment.class.getSimpleName());
        binding.itemsRecyclerView.setNestedScrollingEnabled(false);
        binding.itemsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        binding.itemsRecyclerView.setAdapter(tripsAdapter);

        return binding.getRoot();
    }

    @Override
    public void onItemClick(View view, int index) {
        Intent intent   = new Intent(getActivity() , UserDetailActivity.class);
        startActivity(intent);
    }
}
