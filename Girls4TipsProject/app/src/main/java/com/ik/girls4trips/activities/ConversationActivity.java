package com.ik.girls4trips.activities;

import android.databinding.DataBindingUtil;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ik.girls4trips.Adapters.HomePagerAdapter;
import com.ik.girls4trips.R;
import com.ik.girls4trips.databinding.ActivityConversationBinding;
import com.ik.girls4trips.fragments.ConvoFriendsFragment;
import com.ik.girls4trips.fragments.ConvoPhotoRequestFragment;
import com.ik.girls4trips.fragments.ConvoReceivedFragment;

public class ConversationActivity extends BaseActivity {

    ActivityConversationBinding binding;
    int pagerPos=0;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_conversation);
        binding.setActivity(this);

        init();
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void init() {
        binding.mToolbar1.tvMenuName.setText(getResources().getString(R.string.conversations));
        setupViewPager(binding.viewpager);
    }

    public void onClick(View view) {
        switch (view.getId()){
            case R.id.menu:
                onBackPressed();
                break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setupViewPager(ViewPager viewPager) {

        HomePagerAdapter adapter = new HomePagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ConvoReceivedFragment(), getResources().getString(R.string.received));
        adapter.addFragment(new ConvoFriendsFragment(), getResources().getString(R.string.friends));
        adapter.addFragment(new ConvoPhotoRequestFragment(), getResources().getString(R.string.photo_request));

        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(pagerPos);
        viewPager.setOffscreenPageLimit(3);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.tabs));
        binding.tabs.setupWithViewPager(viewPager);
        try {
            RelativeLayout ll_one = (RelativeLayout) LayoutInflater.from(ConversationActivity.this).inflate(R.layout.tab_custom1, null);
            if (pagerPos == 0) {
            }
            ((TextView) ll_one.findViewById(R.id.name)).setText(getResources().getString(R.string.received));
            binding.tabs.getTabAt(0).setCustomView(ll_one);
            binding.tabs.getTabAt(0).setTag(getResources().getString(R.string.received));

            RelativeLayout ll_two = (RelativeLayout) LayoutInflater.from(ConversationActivity.this).inflate(R.layout.tab_custom1, null);
            if (pagerPos == 1) {
            }
            ((TextView) ll_two.findViewById(R.id.name)).setText(getResources().getString(R.string.friends));
            binding.tabs.getTabAt(1).setCustomView(ll_two);
            binding.tabs.getTabAt(1).setTag(getResources().getString(R.string.friends));

            RelativeLayout ll_three = (RelativeLayout) LayoutInflater.from(ConversationActivity.this).inflate(R.layout.tab_custom1, null);
            if (pagerPos == 2) {
            }
            ((TextView) ll_three.findViewById(R.id.name)).setText(getResources().getString(R.string.photo_request));
            binding.tabs.getTabAt(2).setCustomView(ll_three);
            binding.tabs.getTabAt(2).setTag(getResources().getString(R.string.photo_request));

        }catch (Exception e) {
            e.printStackTrace();
        }
        binding.tabs.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                TextView name = (TextView) ((RelativeLayout) tab.getCustomView()).getChildAt(0);
                //tab.getCustomView().setBackgroundColor(getResources().getColor(R.color.black_gray_83));

                //name.setTextColor(getResources().getColor(R.color.blue_dark));
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                super.onTabUnselected(tab);
                TextView name = (TextView) ((RelativeLayout) tab.getCustomView()).getChildAt(0);
                //tab.getCustomView().setBackgroundColor(getResources().getColor(R.color.black_gray_83));

                //name.setTextColor(getResources().getColor(R.color.blue_dark));
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_out_top);
        finish();
    }

}
